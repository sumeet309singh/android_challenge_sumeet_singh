package com.sumeet.app.ixigo.flight_list.data

import com.google.gson.annotations.SerializedName
import com.sumeet.app.ixigo.data.BaseDto

data class Appendix(
    @SerializedName("airlines") val airlines: Map<String, String>? = null,
    @SerializedName("airports") val airports: Map<String, String>? = null,
    @SerializedName("providers") val providers: Map<Int, String>? = null
) : BaseDto()