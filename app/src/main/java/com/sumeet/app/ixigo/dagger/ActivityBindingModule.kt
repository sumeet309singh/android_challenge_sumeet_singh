package com.sumeet.app.ixigo.dagger

import com.sumeet.app.ixigo.home.di.HomeActivityModule
import com.sumeet.app.ixigo.home.ui.HomeActivity
import com.sumeet.app.ixigo.dagger.scopes.ActivityScope
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
internal abstract class ActivityBindingModule {

    @get:ActivityScope
    @get:ContributesAndroidInjector(modules = [HomeActivityModule::class])
    internal abstract val mainActivity: HomeActivity
}
