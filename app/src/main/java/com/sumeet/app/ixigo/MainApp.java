package com.sumeet.app.ixigo;

import android.content.Context;
import com.facebook.stetho.Stetho;
import com.sumeet.app.ixigo.dagger.AppComponent;
import com.sumeet.app.ixigo.dagger.DaggerAppComponent;
import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;
import timber.log.Timber;

public class MainApp extends DaggerApplication {

    private static MainApp mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        initSdks();
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        AppComponent appComponent = DaggerAppComponent.builder().application(this).build();
        appComponent.inject(this);
        return appComponent;
    }

    private void initSdks() {
        if (BuildConfig.DEBUG_MODE) {
            Stetho.initializeWithDefaults(this);
            Timber.plant(new Timber.DebugTree());
        }
    }

    public static Context getApplicationInstance() {
        return mInstance;
    }
}
