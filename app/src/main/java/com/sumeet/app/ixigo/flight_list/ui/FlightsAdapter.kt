package com.sumeet.app.ixigo.flight_list.ui

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.sumeet.app.ixigo.databinding.FlightItemBinding
import com.sumeet.app.ixigo.databinding.ItemFlightFareBinding
import com.sumeet.app.ixigo.flight_list.data.Fare
import com.sumeet.app.ixigo.flight_list.data.Flight
import com.sumeet.app.ixigo.flight_list.data.FlightFareListItem
import com.sumeet.app.ixigo.flight_list.viewModel.FlightSearchViewModel

/**
 * Adapter for the task list. Has a reference to the [FlightSearchViewModel] to send actions back to it.
 */
class FlightListAdapter(private val viewModel: FlightSearchViewModel) :
    ListAdapter<FlightFareListItem, RecyclerView.ViewHolder>(TaskDiffCallback()) {

    private val flightView: Int = 0

    private val fareView: Int = 1

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = getItem(position)
        when (getItemViewType(position)) {
            fareView -> return (holder as ViewHolderFare).bind(viewModel, item as Fare)
            flightView -> return (holder as ViewHolderFlight).bind(viewModel, item as Flight)
        }
    }

    override fun getItemViewType(position: Int): Int {

        return when (getItem(position)) {
            is Flight -> flightView
            is Fare -> fareView
            else -> error("no other type possible")
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            fareView -> ViewHolderFare.from(parent)
            flightView -> ViewHolderFlight.from(parent)
            else -> ViewHolderFlight.from(parent)
        }
    }

    class ViewHolderFlight private constructor(private val binding: FlightItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        @SuppressLint("SetTextI18n")
        fun bind(viewModel: FlightSearchViewModel, item: Flight) {

            binding.viewmodel = viewModel
            binding.flight = item
            binding.executePendingBindings()
        }

        companion object {

            fun from(parent: ViewGroup): ViewHolderFlight {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = FlightItemBinding.inflate(layoutInflater, parent, false)
                return ViewHolderFlight(binding)
            }
        }
    }


    class ViewHolderFare private constructor(private val binding: ItemFlightFareBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(viewModel: FlightSearchViewModel, item: Fare) {

            binding.viewmodel = viewModel
            binding.fareData = item
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolderFare {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ItemFlightFareBinding.inflate(layoutInflater, parent, false)
                return ViewHolderFare(binding)
            }
        }
    }

}

/**
 * Callback for calculating the diff between two non-null items in a list.
 *
 * Used by ListAdapter to calculate the minimum number of changes between and old list and a new
 * list that's been passed to `submitList`.
 */
class TaskDiffCallback : DiffUtil.ItemCallback<FlightFareListItem>() {
    override fun areItemsTheSame(oldItem: FlightFareListItem, newItem: FlightFareListItem): Boolean {
        return (oldItem.index == newItem.index)
    }

    override fun areContentsTheSame(oldItem: FlightFareListItem, newItem: FlightFareListItem): Boolean {
        return when (oldItem) {
            is Flight -> newItem is Flight && oldItem as Flight == newItem as Flight
            is Fare -> newItem is Fare && oldItem as Fare == newItem as Fare
            else -> false
        }

    }
}
