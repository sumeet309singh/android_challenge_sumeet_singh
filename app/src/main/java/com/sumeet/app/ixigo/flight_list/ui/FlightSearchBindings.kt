package com.sumeet.app.ixigo.flight_list.ui

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.sumeet.app.ixigo.flight_list.data.Flight
import com.sumeet.app.ixigo.flight_list.data.FlightFareListItem

/**
 * [BindingAdapter]s for the [FlightFareListItem]s list.
 */
@BindingAdapter("app:items")
fun setItems(listView: RecyclerView, items: List<FlightFareListItem>) {
    (listView.adapter as FlightListAdapter).submitList(items)
}