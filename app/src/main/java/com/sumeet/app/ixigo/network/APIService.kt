package com.sumeet.app.ixigo.network

import com.sumeet.app.ixigo.flight_list.data.FlightListDto
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET


interface APIService {

    @GET("v2/5979c6731100001e039edcb3")
    suspend fun getFlightsListAsync(): Response<FlightListDto>
}
