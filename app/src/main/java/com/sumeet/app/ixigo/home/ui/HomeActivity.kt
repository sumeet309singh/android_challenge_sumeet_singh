package com.sumeet.app.ixigo.home.ui

import android.os.Bundle
import com.sumeet.app.ixigo.R
import dagger.android.support.DaggerAppCompatActivity

//container activity
class HomeActivity : DaggerAppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

    }
}
