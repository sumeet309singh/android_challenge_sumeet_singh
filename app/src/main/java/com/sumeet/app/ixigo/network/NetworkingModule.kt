package com.sumeet.app.ixigo.network

import android.app.Application

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.readystatesoftware.chuck.ChuckInterceptor

import javax.inject.Singleton

import com.sumeet.app.ixigo.BuildConfig
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
class NetworkingModule {

    @Singleton
    @Provides
    internal fun getOkHttpClientBuilder(application: Application): OkHttpClient {
        return OkHttpClient()
            .newBuilder()
            .addInterceptor(ChuckInterceptor(application))
            .build()
    }

    @Singleton
    @Provides
    internal fun apiService(okhttpClient: OkHttpClient, gson: Gson): APIService {
        val retrofit = Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .client(okhttpClient)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build()
        return retrofit
            .create(APIService::class.java)
    }
}
