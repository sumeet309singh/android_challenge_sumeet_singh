package com.sumeet.app.ixigo.flight_list.data

import com.google.gson.annotations.SerializedName
import com.sumeet.app.ixigo.data.BaseDto

data class Fare(
    @SerializedName("providerId")
    var providerId: Int = 0,
    @SerializedName("fare")
    val fare: Int = 0
) : BaseDto(), FlightFareListItem {

    override var index: Int = -1
}