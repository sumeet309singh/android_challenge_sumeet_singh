package com.sumeet.app.ixigo.home.di

import com.sumeet.app.ixigo.dagger.scopes.FragmentScope
import com.sumeet.app.ixigo.flight_list.di.FlightListModule
import com.sumeet.app.ixigo.flight_list.ui.FlightSearchFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class HomeActivityModule {

    @FragmentScope
    @ContributesAndroidInjector(modules = [FlightListModule::class])
    internal abstract fun getFlightListComponent(): FlightSearchFragment
}