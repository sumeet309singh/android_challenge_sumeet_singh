package com.sumeet.app.ixigo.flight_list.ui


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.sumeet.app.ixigo.R
import com.sumeet.app.ixigo.ViewModelFactory
import com.sumeet.app.ixigo.databinding.FragmentFlightSearchBinding
import com.sumeet.app.ixigo.flight_list.data.FlightListRepo
import com.sumeet.app.ixigo.flight_list.viewModel.FlightSearchViewModel
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_flight_search.*
import javax.inject.Inject


/**
 * A simple [Fragment] subclass.
 * Use the [FlightSearchFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class FlightSearchFragment : DaggerFragment() {

    private lateinit var viewDataBinding: FragmentFlightSearchBinding
    private lateinit var flightlistAdapter: FlightListAdapter
    private lateinit var flightSearchViewModel: FlightSearchViewModel

    @Inject
    lateinit var flightsRepo : FlightListRepo

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        flightSearchViewModel = ViewModelProviders.of(this)[FlightSearchViewModel::class.java]
        viewDataBinding = FragmentFlightSearchBinding.inflate(inflater, container, false)
            .apply {
            viewmodel = flightSearchViewModel }

        return viewDataBinding.root
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         */
        @JvmStatic
        fun newInstance() = FlightSearchFragment()

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        // Set the lifecycle owner to the lifecycle of the view
        viewDataBinding.lifecycleOwner = this.viewLifecycleOwner

        flightlistAdapter = FlightListAdapter(flightSearchViewModel)
        rv_flight_list.layoutManager = LinearLayoutManager(activity).apply { orientation = RecyclerView.VERTICAL }
        rv_flight_list.adapter = flightlistAdapter

        flightSearchViewModel.onUiCreated(flightsRepo)
    }
}