package com.sumeet.app.ixigo.extentions

import androidx.lifecycle.MutableLiveData
import com.sumeet.app.ixigo.flight_list.data.Fare

fun <T : Any?> MutableLiveData<T>.default(initialValue: T) = apply { setValue(initialValue) }

fun List<Fare>.getMinimum(): Int {
    var min = Integer.MAX_VALUE
    for (fare in this) {
        if (fare.fare < min) {
            min = fare.fare
        }
    }
    return min
}