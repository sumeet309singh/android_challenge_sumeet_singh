package com.sumeet.app.ixigo.dagger

import android.app.Application
import android.content.Context
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides

import javax.inject.Named
import javax.inject.Singleton


@Module
internal class AppModule {

    @Provides
    @Singleton
    fun gson(): Gson {
        return GsonBuilder()
            .setLenient()
            .create()
    }

    @Provides
    @Singleton
    fun requestManager(context: Application): RequestManager {
        return Glide.with(context)
    }

    @Provides
    @Singleton
    @Named("ApplicationContext")
    fun context(application: Application): Context {
        return application
    }
}