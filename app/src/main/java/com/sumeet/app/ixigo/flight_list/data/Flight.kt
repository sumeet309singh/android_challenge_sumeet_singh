package com.sumeet.app.ixigo.flight_list.data

import com.google.gson.annotations.SerializedName
import com.sumeet.app.ixigo.data.BaseDto

data class Flight (

    @SerializedName("originCode")
    val originCode: String? = null,
    @SerializedName("destinationCode")
    val destinationCode: String? = null,
    @SerializedName("departureTime")
    val departureTime: Long = 0,
    @SerializedName("arrivalTime")
    val arrivalTime: Long = 0,
    @SerializedName("fares")
    val fares: List<Fare>? = null,
    @SerializedName("airlineCode")
    val airlineCode: String? = null,
    @SerializedName("class")
    val _class: String? = null

): BaseDto(), FlightFareListItem{

    override var index: Int = -1

    var airline : String? = null
    var destination : String? = null
    var origin : String? = null
}