package com.sumeet.app.ixigo.network;

import androidx.annotation.NonNull;

import retrofit2.Response;

public class NetworkResponseException extends Exception {
    @NonNull
    private final Response response;

    public NetworkResponseException(@NonNull Response response) {
        super(response.message());
        this.response = response;
    }

    @NonNull
    public Response getResponse() {
        return this.response;
    }
}
