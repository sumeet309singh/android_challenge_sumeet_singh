package com.sumeet.app.ixigo.flight_list.viewModel

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sumeet.app.ixigo.extentions.default
import com.sumeet.app.ixigo.extentions.getMinimum
import com.sumeet.app.ixigo.flight_list.data.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class FlightSearchViewModel : ViewModel() {

    private lateinit var flightListRepo: FlightListRepo
    private lateinit var appendix: Appendix
    private lateinit var flightOriginal: List<Flight>

    private val _loading = MutableLiveData<Int>().default(View.GONE)
    val loading: LiveData<Int> = _loading

    private val _flights = MutableLiveData<List<FlightFareListItem>>()
    val flightList: LiveData<List<FlightFareListItem>> = _flights.default(ArrayList())

    private fun fetchFlights() {
        _loading.value = View.VISIBLE
        viewModelScope.launch(Dispatchers.IO) {

            val flights = flightListRepo.getFlights()
            flights.let {
                it?.flights?.forEach { flight -> transformFlightDto(flight, it) }
            }
            flights?.appendix?.let { this@FlightSearchViewModel.appendix = it }
            withContext(Dispatchers.Main) {
                flights?.flights?.let {
                    flightOriginal = it
                    _flights.value = makeFlightFareList(it)
                }
                _loading.value = View.GONE
            }
        }
    }

    private fun makeFlightFareList(originalList: List<Flight>): List<FlightFareListItem> {
        val flightFareList = mutableListOf<FlightFareListItem>()
        var index = 0
        for (flightItem in originalList) {
            flightItem.index = index
            index++
            flightFareList.add(flightItem)
            flightItem.fares?.let {
                for (fareItem in it) {
                    flightFareList.add(fareItem)
                    fareItem.index = index
                    index++
                }
            }
        }
        return flightFareList
    }

    private fun transformFlightDto(
        flight: Flight,
        it: FlightListDto
    ) {
        flight.airline = it.appendix?.airlines?.get(flight.airlineCode)
        flight.origin = it.appendix?.airports?.get(flight.originCode)
        flight.destination = it.appendix?.airports?.get(flight.destinationCode)
    }

    fun onUiCreated(flightListRepo: FlightListRepo) {
        this.flightListRepo = flightListRepo
        fetchFlights()

    }

    fun getFormatedTime(time: Long): String {
        return SimpleDateFormat("h:mm a", Locale("en")).format(Date(time))
    }

    fun getProvidedBy(providerId: Int): String {
        return appendix.providers?.get(providerId) ?: error("bad response")
    }

    fun getAmount(amount: Int): String {
        return "Rs $amount"
    }

    class ComparePrice {

        companion object : Comparator<Flight> {

            override fun compare(a: Flight, b: Flight): Int {
                return a.fares!!.getMinimum() - b.fares!!.getMinimum()
            }
        }
    }

    class CompareArrivalTime {

        companion object : Comparator<Flight> {

            override fun compare(a: Flight, b: Flight): Int {
                val comparison = a.arrivalTime - b.arrivalTime
                return when {
                    comparison > 0 -> 1
                    comparison == 0L -> 0
                    comparison < 0 -> -1
                    else -> 0
                }
            }
        }
    }

    class CompareDepartureTime {

        companion object : Comparator<Flight> {

            override fun compare(a: Flight, b: Flight): Int {
                val comparison = a.departureTime - b.departureTime
                return when {
                    comparison > 0 -> 1
                    comparison == 0L -> 0
                    comparison < 0 -> -1
                    else -> 0
                }
            }
        }
    }


    fun sortOnPricing() {

        _flights.value?.let {
            if (it.isNotEmpty()) {
                val sortedList = flightOriginal.toMutableList()
                sortedList.sortWith(ComparePrice)
                _flights.value = makeFlightFareList(sortedList)
            }
        }
    }

    fun sortOnTimeArrival() {

        _flights.value?.let {
            if (it.isNotEmpty()) {
                val sortedList = flightOriginal.toMutableList()
                sortedList.sortWith(CompareArrivalTime)
                _flights.value = makeFlightFareList(sortedList)
            }
        }
    }

    fun sortOnTimeDeparture() {

        _flights.value?.let {
            if (it.isNotEmpty()) {
                val sortedList = flightOriginal.toMutableList()
                sortedList.sortWith(CompareDepartureTime)
                _flights.value = makeFlightFareList(sortedList)
            }
        }
    }

    fun getUnsortedList() {
        _flights.value = makeFlightFareList(flightOriginal)
    }

}


