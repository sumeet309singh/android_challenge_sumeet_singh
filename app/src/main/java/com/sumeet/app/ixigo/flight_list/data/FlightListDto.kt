package com.sumeet.app.ixigo.flight_list.data

import com.google.gson.annotations.SerializedName
import com.sumeet.app.ixigo.data.BaseDto

data class FlightListDto(

    @SerializedName("appendix")
    val appendix: Appendix? = null,
    @SerializedName("flights")
    val flights: List<Flight>? = null

) : BaseDto()