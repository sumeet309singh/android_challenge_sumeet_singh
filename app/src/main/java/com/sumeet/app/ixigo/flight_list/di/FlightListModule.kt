package com.sumeet.app.ixigo.flight_list.di

import com.sumeet.app.ixigo.dagger.scopes.FragmentScope
import com.sumeet.app.ixigo.flight_list.data.FlightListRepo
import com.sumeet.app.ixigo.flight_list.data.FlightListRepoImpl
import com.sumeet.app.ixigo.network.APIService
import dagger.Module
import dagger.Provides

@Module
class FlightListModule {

    @Provides
    @FragmentScope
    fun getRepo(apiService: APIService): FlightListRepo {
        return FlightListRepoImpl(apiService)
    }
}