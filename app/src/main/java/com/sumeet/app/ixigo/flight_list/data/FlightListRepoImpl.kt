package com.sumeet.app.ixigo.flight_list.data

import com.sumeet.app.ixigo.network.APIService
import com.sumeet.app.ixigo.network.BaseRepository

class FlightListRepoImpl(private val apiInterface: APIService) : BaseRepository(), FlightListRepo {

    override suspend fun getFlights(): FlightListDto? {

        return safeApiCall(
            call = { apiInterface.getFlightsListAsync() },
            errorMessage = "Error Fetching Popular Movies"
        )

    }
}