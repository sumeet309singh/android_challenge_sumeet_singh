package com.sumeet.app.ixigo.dagger;

import android.app.Application;
import com.sumeet.app.ixigo.MainApp;
import com.sumeet.app.ixigo.network.NetworkingModule;
import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

import javax.inject.Singleton;

@Singleton
@Component(modules = {
        AndroidSupportInjectionModule.class,
        NetworkingModule.class,
        AppModule.class,
        ActivityBindingModule.class
})

public interface AppComponent extends AndroidInjector<MainApp> {

    @Override
    void inject(MainApp instance);

    @Component.Builder
    interface Builder {
        @BindsInstance
        AppComponent.Builder application(Application application);

        AppComponent build();
    }
}

